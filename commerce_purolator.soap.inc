<?php

/**
 * @file
 * Handles XML-related stuff for Commerce Purolator module.
 */

/**
 * This builds the request object to submit to Purolator for rates.
 */
function commerce_purolator_build_full_rate_request_for_order($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Determine the shipping profile reference field name for the order.
  $field_name = commerce_physical_order_shipping_field_name($order);
  $shipping_profile = $order_wrapper->{$field_name}->value();

  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->{$field_name}->commerce_customer_address)) {
    $shipping_address = $order_wrapper->{$field_name}->commerce_customer_address->value();
  }
  else {
    $shipping_address = addressfield_default_values();
  }
  // This returns $weight['unit'] and $weight['weight']
  $weight = commerce_physical_order_weight($order, 'lb');
  $num_packages = commerce_purolator_num_shippable_line_items($order);

  return commerce_purolator_build_full_rate_request($shipping_address, $weight, $num_packages);
}
/**
 * This builds the request object to submit to Purolator for rates.
 */
function commerce_purolator_build_full_rate_request($shipping_address, $weight, $num_packages, $date = NULL) {
  /* If there is no total weight for the order, there is no reason to send the request to Purolator */
  if ($weight['weight'] == NULL) {
    return FALSE;
  }

  $api_vars = commerce_purolator_decrypt_vars(TRUE);

  // Populate the Origin Information.
  $request->Shipment->SenderInformation->Address->Name = variable_get('commerce_purolator_company_name');
  $request->Shipment->SenderInformation->Address->StreetNumber = variable_get('commerce_purolator_street_number');
  $request->Shipment->SenderInformation->Address->StreetName = variable_get('commerce_purolator_street_name');
  $request->Shipment->SenderInformation->Address->City = variable_get('commerce_purolator_city');
  $request->Shipment->SenderInformation->Address->Province = variable_get('commerce_purolator_state');
  $request->Shipment->SenderInformation->Address->Country = variable_get('commerce_purolator_country_code');
  $request->Shipment->SenderInformation->Address->PostalCode = variable_get('commerce_purolator_postal_code');
  // Populate the Desination Information.
  $request->Shipment->ReceiverInformation->Address->Name = $shipping_address['name_line'];
  // $request->Shipment->ReceiverInformation->Address->StreetNumber = "2245";
  $request->Shipment->ReceiverInformation->Address->StreetName = $shipping_address['thoroughfare'];
  $request->Shipment->ReceiverInformation->Address->City = $shipping_address['locality'];
  $request->Shipment->ReceiverInformation->Address->Province = $shipping_address['administrative_area'];
  $request->Shipment->ReceiverInformation->Address->Country = $shipping_address['country'];
  $request->Shipment->ReceiverInformation->Address->PostalCode = $shipping_address['postal_code'];

  // Future Dated Shipments - YYYY-MM-DD format.
  $request->Shipment->ShipmentDate = $date ? date('Y-m-d', $date) : date('Y-m-d');

  // Populate the Package Information.
  $request->Shipment->PackageInformation->TotalWeight->Value = $weight['weight'];
  $request->Shipment->PackageInformation->TotalWeight->WeightUnit = $weight['unit'];
  $request->Shipment->PackageInformation->TotalPieces = $num_packages;
  // The ServiceID doesn't seem to actually do anything, but is required.
  $request->Shipment->PackageInformation->ServiceID = "PurolatorExpress";
  // Populate the Payment Information.
  $request->Shipment->PaymentInformation->PaymentType = "Sender";
  $request->Shipment->PaymentInformation->BillingAccountNumber = variable_get('commerce_purolator_billing_account_id');
  $request->Shipment->PaymentInformation->RegisteredAccountNumber = variable_get('commerce_purolator_account_id');
  // Populate the Pickup Information.
  $request->Shipment->PickupInformation->PickupType = "DropOff";
  $request->ShowAlternativeServicesIndicator = "true";
  // OriginSignatureNotRequired.
  $request->Shipment->PackageInformation->OptionsInformation->Options->OptionIDValuePair->ID = "OriginSignatureNotRequired";
  $request->Shipment->PackageInformation->OptionsInformation->Options->OptionIDValuePair->Value = "true";

  return $request;
}

/**
 * Builds an API request for the Purolator webservice
 *
 * @param array $shipping_address
 *   An array of address information
 * @param array $weight
 *   Weight information in the form of $weight['weight'] and $weight['unit']
 */
function commerce_purolator_build_quick_rate_request($shipping_address, $weight, $date = NULL) {


  $purolator_pickupschedule = variable_get('commerce_purolator_pick_up_schedule');

  /* If there is no total weight for the order, there is no reason to send the request to Purolator */
  if ($weight['weight'] == NULL) {
    return FALSE;
  }

  $api_vars = commerce_purolator_decrypt_vars(TRUE);

  // Populate the Origin Information. We only need the postal code for a quick
  // estimate.
  $request->SenderPostalCode = variable_get('commerce_purolator_postal_code');
  // Populate the Desination Information.
  $request->ReceiverAddress->City = $shipping_address['locality'];
  $request->ReceiverAddress->Province = $shipping_address['administrative_area'];
  $request->ReceiverAddress->Country = $shipping_address['country'];
  $request->ReceiverAddress->PostalCode = $shipping_address['postal_code'];

  // Future Dated Shipments - YYYY-MM-DD format.
  $request->Shipment->ShipmentDate = $date ? date('Y-m-d', $date) : date('Y-m-d');
  $request->BillingAccountNumber = variable_get('commerce_purolator_billing_account_id');

  // Populate the Package Information.
  $request->PackageType = "CustomerPackaging";

  // Populate the Package Information.
  $request->TotalWeight->Value = $weight['weight'];
  $request->TotalWeight->WeightUnit = $weight['unit'];
  $request->Shipment->PaymentInformation->BillingAccountNumber = variable_get('commerce_purolator_billing_account_id');

  return $request;
}

/**
 * Submits an API request to the Purolator webservice
 *
 * @param object $request_object
 *   An XML string to submit to the Progistics XML Processor.
 * @param string $message
 *   Optional log message to use for logged API requests.
 */
function commerce_purolator_api_request($request_object, $type = 'full', $message = '') {
  try{
    // Get the SOAP client.
    $client = commerce_purolator_create_pws_client();
    // Execute the request and capture the response.
    if($type == 'quick') {
      $response = $client->GetQuickEstimate($request_object);
    }
    else {
      $response = $client->GetFullEstimate($request_object);
    }
  }catch(SoapFault $e){
    watchdog('purolator', 'SoapFault(@code): @error', array('@code' => $e->faultcode, '@error' => $e->faultstring), WATCHDOG_ERROR);
    $errors = array((object) array(
      'Code' => $e->faultcode,
      'Description' => $e->faultstring,
    ));
    return array('Errors' => $errors);
  }

  // Log any errors to the watchdog.
  if (!$response->ShipmentEstimates) {
    $errors = $response->ResponseInformation->Errors->Error;
    if (!is_array($errors)) {
      $errors = array($errors);
    }
    foreach ($errors as $error) {
      watchdog(
        'purolator',
        'PWS @error_type(@code): @error',
        array(
          '@error_type' => $error->AdditionalInformation,
          '@code' => $error->Code,
          '@error' => $error->Description,
        ),
        WATCHDOG_ERROR);
    }
    return array('Errors' => $errors);
  }
  else {
    $estimates = array();
    foreach ($response->ShipmentEstimates->ShipmentEstimate as $estimate) {
      $estimates[$estimate->ServiceID] = $estimate;
    }
    return $estimates;
  }
}
/**
 * Helper function to create the purolation web services client
 */
function commerce_purolator_create_pws_client($type = "EstimatingService") {
  $services = _commerce_purolator_soap_service_list();
  // Set the parameters for the Non-WSDL mode SOAP communication with your
  // Development/Production credentials.
  $client = new SoapClient(drupal_get_path('module', 'commerce_purolator') . "/wsdl/" . $services[$type]['wsdl'],
    array(
      'trace'			=>	TRUE,
      'location'	=>	$services[$type]['location'],
      // 'location'	=>	"https://webservices.purolator.com/PWS/V1/Estimating/EstimatingService.asmx",
      'uri'				=>	"http://purolator.com/pws/datatypes/v1",
      'login'			=>	variable_get('commerce_purolator_access_key'),
      'password'	=>	variable_get('commerce_purolator_password'),
    )
  );
  // Define the SOAP Envelope Headers.
  $headers[] = new SoapHeader('http://purolator.com/pws/datatypes/v1',
    'RequestContext',
    array(
      'Version'           =>  '1.3',
      'Language'          =>  'en',
      'GroupID'           =>  'xxx',
      'RequestReference'  =>  'Commerce ' . $type,
    )
  );
  // Apply the SOAP Header to your client.
  $client->__setSoapHeaders($headers);
  return $client;
}
