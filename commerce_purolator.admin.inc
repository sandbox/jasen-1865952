<?php

/**
 * @file
 * Handles admin settings page for Commerce Purolator module.
 */

/* hook_settings_form() */
function commerce_purolator_settings_form($form, &$form_state) {
  $purolator_link = l(t('Purolator.com'), 'https://eship.purolator.com/SITE/en/createnewuser.aspx', array('attributes' => array('target' => '_blank')));
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Purolator API credentials'),
    '#collapsible' => TRUE,
    '#description' => t('In order to obtain shipping rate estimates, you must have an account with Purolator. You can apply for Purolator E-Ship API credentials at !purolator', array('!purolator' => $purolator_link)),
  );

  $encrypted = variable_get('commerce_purolator_encrypt', FALSE);
  $api_vars = commerce_purolator_decrypt_vars(FALSE);

  $form['api']['commerce_purolator_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Account ID'),
    '#default_value' => $api_vars['purolator_accountid'],
    '#required' => TRUE,
  );
  $form['api']['commerce_purolator_billing_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Billing Account ID'),
    '#default_value' => $api_vars['purolator_billing_accountid'],
    '#required' => TRUE,
  );
  $form['api']['commerce_purolator_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Key'),
    '#default_value' => $api_vars['purolator_accesskey'],
    '#required' => TRUE,
  );
  $form['api']['commerce_purolator_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => !variable_get('commerce_purolator_password', FALSE),
    '#description' => t('Please leave blank if you do not want to update your password at this time.'),
  );
  $form['api']['encryption'] = array(
    '#type' => 'item',
    '#title' => t('Encryption'),
    'status' => array(
      '#type' => 'item',
      '#title' => FALSE,
    ),
  );
  if (commerce_purolator_encryption_available(array('display_all' => TRUE))) {
    $form['api']['encryption']['status']['#markup'] =
      'Encryption is available and configured properly.';
    $form['api']['encryption']['commerce_purolator_encrypt'] = array(
      '#type' => 'checkbox',
      '#title' => t('Encrypt Purolator credentials (HIGHLY RECOMMENDED)'),
      '#description' => t('Note: Enabling this setting automatically will encrypt your password even though you cannot see it. Disabling this checkbox requires that you re-enter a password.'),
      '#default_value' => $encrypted,
    );
  }
  else {
    $aes_link = l(t('AES'), 'http://drupal.org/project/aes', array('attributes' => array('target' => '_blank')));
    $form['api']['encryption']['status']['#markup'] = '<span class="error">' . t('!aes is not installed - your login credentials will not be encrypted.', array('!aes' => $aes_link)) . '</span>';
  }

  $form['origin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ship From Address'),
    '#collapsible' => TRUE,
  );
  $form['origin']['commerce_purolator_company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#default_value' => variable_get('commerce_purolator_company_name')
  );
  $form['origin']['commerce_purolator_street_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Street number'),
    '#size' => 15,
    '#default_value' => variable_get('commerce_purolator_street_number')
  );
  $form['origin']['commerce_purolator_street_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Street name'),
    '#default_value' => variable_get('commerce_purolator_street_name')
  );
  $form['origin']['commerce_purolator_address_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address (Additional)'),
    '#default_value' => variable_get('commerce_purolator_address_2')
  );
  $form['origin']['commerce_purolator_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => variable_get('commerce_purolator_city')
  );
  $form['origin']['commerce_purolator_state'] = array(
    '#type' => 'select',
    '#title' => t('State/Provice'),
    '#default_value' => variable_get('commerce_purolator_state'),
    '#options' => array(
      '' => t('Please Select'),
      'Canada' => array(
      'AB' => t('Alberta'),
      'BC' => t('British Columbia'),
      'MB' => t('Manitoba'),
      'NB' => t('New Brunswick'),
      'NL' => t('Newfoundland and Labrador'),
      'NT' => t('Northwest Territories'),
      'NS' => t('Nova Scotia'),
      'NU' => t('Nunavut'),
      'ON' => t('Ontario'),
      'PE' => t('Prince Edward Island'),
      'QC' => t('Quebec'),
      'SK' => t('Saskatchewan'),
      'YT' => t('Yukon'),
      ),
      'United States' => array(
      'AL' => t('Alabama'),
      'AK' => t('Alaska'),
      'AZ' => t('Arizona'),
      'AR' => t('Arkansas'),
      'CA' => t('California'),
      'CO' => t('Colorado'),
      'CT' => t('Connecticut'),
      'DE' => t('Delaware'),
      'DC' => t('District of Columbia'),
      'FL' => t('Florida'),
      'GA' => t('Georgia'),
      'HI' => t('Hawaii'),
      'ID' => t('Idaho'),
      'IL' => t('Illinois'),
      'IN' => t('Indiana'),
      'IA' => t('Iowa'),
      'KS' => t('Kansas'),
      'KY' => t('Kentucky'),
      'LA' => t('Louisiana'),
      'ME' => t('Maine'),
      'MD' => t('Maryland'),
      'MA' => t('Massachusetts'),
      'MI' => t('Michigan'),
      'MN' => t('Minnesota'),
      'MS' => t('Mississippi'),
      'MO' => t('Missouri'),
      'MY' => t('Montana'),
      'NE' => t('Nebraska'),
      'NV' => t('Nevada'),
      'NH' => t('New Hampshire'),
      'NJ' => t('New Jersey'),
      'NM' => t('New Mexico'),
      'NY' => t('New York'),
      'NC' => t('North Carolina'),
      'ND' => t('North Dakota'),
      'OH' => t('Ohio'),
      'OK' => t('Oklahoma'),
      'OR' => t('Oregon'),
      'PA' => t('Pennsylvania'),
      'RI' => t('Rhode Island'),
      'SC' => t('South Carolina'),
      'SD' => t('South Dakota'),
      'TN' => t('Tennessee'),
      'TX' => t('Texas'),
      'UT' => t('Utah'),
      'VT' => t('Vermont'),
      'VA' => t('Virginia'),
      'WA' => t('Washington'),
      'WV' => t('West Virginia'),
      'WI' => t('Wisconsin'),
      'WY' => t('Wyoming'),
      ),
    )
  );
  $form['origin']['commerce_purolator_postal_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#size' => 6,
    '#default_value' => variable_get('commerce_purolator_postal_code')
  );
  $form['origin']['commerce_purolator_country_code'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => variable_get('commerce_purolator_country_code'),
    '#options' => array(
      '' => t('Please Select'),
      'CA' => t('Canada'),
      'US' => t('United States of America'),
    )
  );
  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable Purolator Shipping Services'),
    '#collapsible' => TRUE,
  );

  foreach (_commerce_purolator_service_list() as $key => $service) {
    $array_options[$key] = $service['title'];
  }
  $form['services']['commerce_purolator_services'] = array(
    '#type' => 'checkboxes',
    '#options' => $array_options,
    '#default_value' => variable_get('commerce_purolator_services', array())
  );
  $form['packaging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable Purolator Packaging Types'),
    '#collapsible' => FALSE,
  );
  $form['packaging']['commerce_purolator_packaging_explanation'] = array(
    '#type' => 'markup',
    '#markup' => t('Only "Customer supplied packaging" is currently available. Please enter the default package size below.'),
  );
  $form['packaging']['commerce_purolator_packaging'] = array(
    '#type' => 'hidden',
    '#value' => '02',
  );
  /*
  $form['packaging']['commerce_purolator_packaging'] = array(
    '#type' => 'checkboxes',
    '#options' => _commerce_purolator_packaging_types(),
    '#default_value' => variable_get('commerce_purolator_packaging', array())
  );
  */
  // Fields for default package size (inches)
  $form['default_package_weight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default package weight'),
    '#collapsible' => FALSE,
    '#description' => 'Purolator requires a package weight when determining estimates.',
  );
  $form['default_package_weight']['commerce_purolator_default_package_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#size' => 5,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_purolator_default_package_weight'),
  );
  $form['default_package_weight']['commerce_purolator_default_package_weight_units'] = array(
    '#type' => 'select',
    '#title' => t('Units'),
    '#options' => array(
      'kg' => t('Kilograms'),
      'lb' => t('Pounds'),
    ),
    '#default_value' => variable_get('commerce_purolator_default_package_weight_units'),
  );
  $form['commerce_purolator_pick_up_schedule'] = array(
    '#type' => 'select',
    '#title' => t('Pick-up Schedule'),
    '#options' => _commerce_purolator_pickup_types(),
    '#default_value' => variable_get('commerce_purolator_pick_up_schedule')
  );
  $form['commerce_purolator_shipto_residential'] = array(
    '#type' => 'checkbox',
    '#title' => t('Calculate ALL ship to addresses as residential.'),
    '#default_value' => variable_get('commerce_purolator_shipto_residential', 0),
  );
  $form['commerce_purolator_show_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Purolator Logo on Shipping Page'),
    '#default_value' => variable_get('commerce_purolator_show_logo', 0),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/*
 * Implements hook_form_validate().
 */
function commerce_purolator_settings_form_validate($form, &$form_state) {
  $encrypted = variable_get('commerce_purolator_encrypt', FALSE) && function_exists('aes_decrypt');
  $values = $form_state['values'];

  // If the Password field is empty, then they're not trying to update it and we should ignore it.
  if (empty($values['commerce_purolator_password'])) {
    unset($form_state['values']['commerce_purolator_password']);
    if ($encrypted && empty($form_state['input']['commerce_purolator_password'])) {
      form_set_error('commerce_purolator_password', t('You must enter in a password to turn off encryption.'));
    }
    return;
  }

  /* If we are setting or resetting values, test the connection.
  $purolator_connection = purolator_api_function_here();
  if (is_good_connection($purolator_connection)) {
    drupal_set_message(t('Connection established. Purolator credentials updated.'));
  }
  else {
    drupal_set_message(
      t('Resetting Purolator credentials failed.'), 'error');
    form_set_error('commerce_purolator_account_id', t('Unable to connect to Purolator. Please check your credentials.'));
    form_set_error('commerce_purolator_user_id');
    form_set_error('commerce_purolator_access_key');
    form_set_error('commerce_purolator_password');
  }
  */
}

/*
 * Implements hook_form_submit().
 */
function commerce_purolator_settings_form_submit($form, &$form_state) {
  $encrypted = variable_get('commerce_purolator_encrypt', FALSE) && function_exists('aes_decrypt');
  // Encrypt the Purolator API credentials if available
  if (isset($form_state['input']['commerce_purolator_encrypt']) && $form_state['input']['commerce_purolator_encrypt']) {
    $form_state['input']['commerce_purolator_user_id'] = commerce_purolator_encrypt($form_state['input']['commerce_purolator_user_id']);
    $form_state['input']['commerce_purolator_account_id'] = commerce_purolator_encrypt($form_state['input']['commerce_purolator_account_id']);
    $form_state['input']['commerce_purolator_access_key'] = commerce_purolator_encrypt($form_state['input']['commerce_purolator_access_key']);
    if (!empty($form_state['input']['commerce_purolator_password'])) {
      $form_state['input']['commerce_purolator_password'] = commerce_purolator_encrypt($form_state['input']['commerce_purolator_password']);
    }
    elseif ($encrypted == FALSE) {
      // This is an odd case, if encryption is turned on but the password is already in the db
      // Then we need to pull it from the db, not from the field
      $form_state['input']['commerce_purolator_password'] = commerce_purolator_encrypt(variable_get('commerce_purolator_password', ''));
    }
  }

  if (empty($form_state['input']['commerce_purolator_password'])) {
    unset($form_state['input']['commerce_purolator_password']);
  }

  if (!isset($form_state['input']['commerce_purolator_encrypt'])) {
    $form_state['input']['commerce_purolator_encrypt'] = FALSE;
  }

  $fields = array(
    'commerce_purolator_account_id',
    'commerce_purolator_billing_account_id',
    'commerce_purolator_password',
    'commerce_purolator_access_key',
    'commerce_purolator_encrypt',
    'commerce_purolator_services',
    'commerce_purolator_company_name',
    //'commerce_purolator_address_line_1',
    'commerce_purolator_street_number',
    'commerce_purolator_street_name',
    'commerce_purolator_address_2',
    'commerce_purolator_city',
    'commerce_purolator_state',
    'commerce_purolator_postal_code',
    'commerce_purolator_country_code',
    'commerce_purolator_packaging',
    'commerce_purolator_pick_up_schedule',
    'commerce_purolator_show_logo',
    'commerce_purolator_default_package_weight',
//    'commerce_purolator_default_package_size_length',
//    'commerce_purolator_default_package_size_width',
//    'commerce_purolator_default_package_size_height',
    'commerce_purolator_shipto_residential',
  );

  foreach ($fields as $key) {
    if (array_key_exists($key, $form_state['input'])) {
      $value = $form_state['input'][$key];
      variable_set($key, $value);
    }
  }

  drupal_set_message(t('The Purolator configuration options have been saved.'));
}
